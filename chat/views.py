from django.http import HttpResponseForbidden
from rest_framework.authentication import TokenAuthentication
from rest_framework.decorators import api_view, authentication_classes, permission_classes
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response

from account.models import Account
from account.serializer import AccountChatSerializer


@api_view(['POST', 'GET', 'PUT', 'DELETE'])
@authentication_classes([TokenAuthentication])
@permission_classes([IsAuthenticated])
def chat_room(request):
    if request.method == 'GET':
        try:
            accounts = Account.objects.all().exclude(id=request.user.id)
            return Response({
                "result": 1,
                "account": AccountChatSerializer(accounts, many=True).data,
                "error": ""
            })
        except Exception as e:
            print(e)
            return Response({
                "result": 0,
                "error": f"{e}"
            })
