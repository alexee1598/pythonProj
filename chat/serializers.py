from django.shortcuts import get_object_or_404
from rest_framework.fields import CharField
from rest_framework.serializers import ModelSerializer

from account.models import Account
from chat.models import MessageModel


class MessageModelSerializer(ModelSerializer):
    user = CharField(source='user.username', read_only=True)
    recipient = CharField(source='recipient.username')

    def create(self, validated_data):
        user = self.context['request'].user
        recipient = get_object_or_404(Account, username=validated_data['recipient']['username'])
        msg = MessageModel(recipient=recipient, body=validated_data['body'], user=user)
        msg.save()
        return msg

    class Meta:
        model = MessageModel
        fields = ('id', 'user', 'recipient', 'timestamp', 'body')


