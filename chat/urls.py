from django.urls import path

from chat import views

app_name = 'chat'

urlpatterns = [
    path('', views.chat_room, name='chat'),
    # path('room/<int:room_id>/', views.chat_room, name='chat_room'),
]
