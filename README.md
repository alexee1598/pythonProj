## Redis ##
    1. run docker -p 6379:6379 redis

## Django ##
    2. python manage.py runserver

## Task worker ###
#### Celery or Dramatiq ####

    CELERY:
    3. celery worker -A ProjectDjango --loglevel=info --pool=solo
    4. celery -A ProjectDjango beat --loglevel=info --pool=solo
    5. celery -A ProjectDjango flower --loglevel=info

    DRAMATIQ:
    3. python manage.py rundramatiq
