# Dockerfile

# Pull base image
FROM python:3.8.3

# Set environment variable
ENV PYTHONDONTWRITEBYTECODE 1
ENV PYTHONUNBUFFERED 1

# Create WORKDIR
WORKDIR /usr/src/app

# Copy requirements and install
RUN pip install --upgrade pip
COPY requirements.txt .
RUN pip install -r requirements.txt
COPY . .
