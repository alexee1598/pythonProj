from rest_framework import serializers

from account.models import Account


class AccountSerializer(serializers.ModelSerializer):
    class Meta:
        model = Account
        fields = ('age', 'description', 'email', 'first_name', 'id', 'image',
                  'last_name', 'username', 'is_active',)


class AccountChatSerializer(serializers.ModelSerializer):
    class Meta:
        model = Account
        fields = ('first_name', 'last_name', 'username')
