from django.contrib.auth.tokens import PasswordResetTokenGenerator


class TokenGenerator(PasswordResetTokenGenerator):
    def __str__(self):
        print(self.key_salt)


account_activation_token = TokenGenerator()
