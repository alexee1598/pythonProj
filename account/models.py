import os

from django.contrib.auth.models import User, AbstractUser, UserManager
from django.contrib.auth.validators import UnicodeUsernameValidator
from django.core.files.storage import FileSystemStorage
from django.core.mail import send_mail
from django.db import models
from django.db.models.signals import pre_save
from django.dispatch import receiver
from django.utils import timezone
from django.utils.deconstruct import deconstructible
from django.utils.translation import gettext_lazy as _

fs = FileSystemStorage(location='/media/photos')


@deconstructible
class UploadPath(object):

    def __call__(self, instance, filename):
        return os.path.join(str(instance.username), filename)


upload_path = UploadPath()


def upload_to(instance, filename):
    return '/'.join(['avatars', ])


class Account(AbstractUser):
    username_validator = UnicodeUsernameValidator()

    username = models.CharField(
        _('username'),
        max_length=150,
        unique=True,
        help_text=_('Required. 150 characters or fewer. Letters, digits and @/./+/-/_ only.'),
        validators=[username_validator],
        error_messages={
            'unique': _("A user with that username already exists."),
        },
    )
    first_name = models.CharField(max_length=30, blank=True)
    last_name = models.CharField(max_length=150, blank=True)
    email = models.EmailField(blank=False, unique=True)
    is_staff = models.BooleanField(default=False)
    is_active = models.BooleanField(_('active'), default=True, help_text=_(
            'Designates whether this user should be treated as active. '
            'Unselect this instead of deleting accounts.'
        ),
    )
    date_joined = models.DateTimeField(default=timezone.now)
    age = models.PositiveSmallIntegerField(null=True, blank=True)
    description = models.TextField(max_length=1000, null=True, blank=True)
    image = models.ImageField(upload_to=upload_path, null=True, blank=True)

    objects = UserManager()

    EMAIL_FIELD = 'email'
    USERNAME_FIELD = 'username'
    REQUIRED_FIELDS = ['email']

    class Meta:
        verbose_name_plural = 'Account'
        verbose_name = 'Accounts'

    def get_full_name(self):
        """
        Return the first_name plus the last_name, with a space in between.
        """
        full_name = '%s %s' % (self.first_name, self.last_name)
        return full_name.strip()

    def get_short_name(self):
        """Return the short name for the user."""
        return self.first_name

    def email_user(self, subject, message, from_email=None, **kwargs):
        """Send an email to this user."""
        send_mail(subject, message, from_email, [self.email], **kwargs)


class SocialAuth(models.Model):
    account = models.ForeignKey(Account, on_delete=models.CASCADE, related_name='account')
    social_id = models.FloatField(unique=True)
    provider = models.CharField(max_length=20)
    created = models.DateTimeField(auto_now_add=True)

    class Meta:
        verbose_name_plural = 'SocialAuth'
        verbose_name = 'SocialsAuth'


class Image(models.Model):
    title = models.CharField(max_length=70, default='default')
    image = models.ImageField(upload_to=upload_path, storage=fs, null=True, blank=True)
    created = models.DateTimeField(auto_now=True)
    updated = models.DateTimeField(auto_now_add=True)
