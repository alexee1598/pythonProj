from venv import logger

import dramatiq
from django.core.mail import EmailMultiAlternatives
from django.template.loader import render_to_string
from django.utils.encoding import force_bytes
from django.utils.http import urlsafe_base64_encode

from account.models import Account
from account.tokens import account_activation_token


@dramatiq.actor(priority=9)
def send_email_registration_job(account_id):
    try:
        mail_subject = 'Confirm your email address.'
        current_site = 'http://localhost:8000'
        account = Account.objects.get(id=account_id)
        uid = urlsafe_base64_encode(force_bytes(account.pk))
        token = account_activation_token.make_token(account)
        activation_link = "{0}/account/activate/{1}/{2}".format(current_site, uid, token)

        html_content = render_to_string('email/confirm-email.html',
                                        {'account': account,
                                         'activation_link': activation_link})
        text_content = 'This is an important message.'
        email = EmailMultiAlternatives(mail_subject, text_content, to=[account.email])
        email.attach_alternative(html_content, "text/html")
        email.send()
        logger.info('All ok')
    except Exception as e:
        logger.error(f'Error - {e}')
