from django.urls import path

from account import views

urlpatterns = [
    path('activate/<str:uid>/<str:token>', views.activate, name='activate_account'),
    path('update-account/', views.account_view, name='update_account'),
    path('change-email/', views.change_email, name='change_email'),
    path('change-password/', views.change_password, name='change_password'),
    path('delete-account/', views.account_view, name='delete_account'),
    path('login/', views.login, name='login'),
    path('register/', views.register_user, name='registration'),
    path('social-register/', views.social_register_user, name='social_registration'),
    path('account/<int:account_id>', views.get_account, name='account_detail'),
]
