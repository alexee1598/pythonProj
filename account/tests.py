from django.core import mail
from django.test import override_settings
from django.urls import reverse
from django_dramatiq.test import DramatiqTestCase
from rest_framework import status
from rest_framework.test import APITestCase, APIRequestFactory

from account.models import Account
from account.tasks import send_email_registration_job


class UserCRUDTest(APITestCase):
    def setUp(self):
        self.car_id = 3000
        self.owner_id = 4000
        self.MAIN_URL = '/account/'
        self.factory = APIRequestFactory()

        self.client.post(reverse("registration"),
                         {'username': 'Test', 'first_name': 'test', 'last_name': 'Test_last_name',
                          'age': 22, 'password': '123456', 'is_active': True, 'description': 'Descr',
                          'email': 'test@email.com'})

    def test_register_user(self):
        response = self.client.post(reverse("registration"),
                                    {'username': 'Test2', 'first_name': 'test2', 'last_name': 'Test_last_name2',
                                     'age': 22, 'password': '123456', 'description': 'Descr', 'email': 'tes3t@tm.com'})
        acc = Account.objects.get(username='Test2')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data.get('result'), 1)
        self.assertIsNotNone(response.data.get('token'))
        self.assertIsNotNone(acc)

    def test_incorrect_create_user_uniq_email(self):
        response = self.client.post(reverse("registration"),
                                    {'username': 'Testgth', 'first_name': 'test',
                                     'last_name': 'last_test', 'password': '123456',
                                     'email': 'test@email.com'},
                                    format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data.get('result'), 0)
        self.assertEqual(response.data.get('error'), 'Email address must be unique')
        acc = Account.objects.filter(username='Test2')
        self.assertIsNotNone(acc)

    def test_login_yer(self):
        response = self.client.post(reverse("login"), {'username': 'Test', 'password': '123456'},
                                    format='json')

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data.get('result'), 1)
        self.assertIsNone(response.data.get('error'))
        self.assertIsNotNone(response.data.get('token'))
        self.assertIsNotNone(response.data.get('account'))

    def test_incorrect_create_user_uniq_username(self):
        response = self.client.post(reverse("registration"),
                                    {'username': 'Test', 'first_name': 'test', 'last_name': 'last test',
                                     'password': '123456', 'email': 'tefsfwefgst@tma.com'},
                                    format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data.get('result'), 0)
        print(response.data.get('error'))
        self.assertEqual(response.data.get('error'), 'Username must be unique')
        acc = Account.objects.filter(username='Test')
        self.assertIsNotNone(acc)


class CustomerTestCase(DramatiqTestCase):

    @override_settings(EMAIL_BACKEND='django.core.mail.backends.locmem.EmailBackend')
    def test_customers_can_be_emailed(self):
        response = self.client.post(reverse("registration"),
                                    {'username': 'Test', 'first_name': 'test', 'last_name': 'last test',
                                     'password': '123456', 'email': 'test@tma.com'}, format='json')

        send_email_registration_job.send(response.data['account']['id'])
        self.broker.join(send_email_registration_job.queue_name)
        self.worker.join()

        self.assertEqual(len(mail.outbox), 2)
        self.assertEqual(mail.outbox[0].subject, "Confirm your email address.")
        # Assuming "send_welcome_email" enqueues an "email_customer" task
