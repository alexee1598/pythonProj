import logging
import re
import secrets
import string

from django.contrib.auth.hashers import make_password
from django.utils import timezone
from email_validator import validate_email, EmailNotValidError
from rest_framework.response import Response

from account.models import SocialAuth, Account

logger = logging.getLogger(__name__)


def random_password():
    alphabet = string.ascii_letters + string.digits
    password = ''.join(secrets.choice(alphabet) for i in range(20))

    return password


def registration(request):
    account = Account()
    account.email = request.data['email']
    account.username = request.data.get('username', '')
    account.age = request.data.get('age', )
    account.description = request.data.get('description', '')
    account.first_name = request.data.get('first_name', '')
    account.last_name = request.data.get('last_name', '')
    account.last_login = timezone.now()
    account.image = request.data.get('avatar', '')

    if request.data.get('password') is not None:
        account.is_active = False
        account.password = make_password(request.data['password'])
    else:
        account.first_name = request.data['firstName']
        account.last_name = request.data['lastName']
        account.username = request.data['email']
        account.password = make_password(random_password())

    account.save()
    return account


def register_user_with_social(request, account):
    social = SocialAuth()
    social.social_id = request.data['id']
    social.provider = request.data['provider']
    social.user = account
    social.save()


def check_valid_email(email):
    try:
        validate_email(email)

        return Response({
            "result": 1,
            "Error": ''
        })

    except EmailNotValidError as e:
        logger.exception(e)

    except Exception as e:
        logger.exception(e)

    return Response({
        "result": 0,
        "error": "It's invalid email domain"
    })


def _update_account(account, request):
    try:
        account.first_name = request.data['first_name']
        account.last_name = request.data['last_name']
        account.age = request.data['age']
        account.description = request.data['description']
        account.image = request.data['image']
        account.save()

        return Response({
            "result": 1,
            "Error": ''
        })

    except Exception as e:
        logger.exception(f'Update_account - {e}')

    return Response({
        "result": 0,
        "error": "Error update account"
    })


def _delete_account(request):
    try:
        a = Account.objects.get(id=request.data['id'])
        a.delete()

        return Response({
            "result": 1,
            "Error": ''
        })

    except Exception as e:
        logger.exception(f'Delete_account - {e}')

    return Response({
        "result": 0,
        "error": "Error delete account"
    })


def _get_account_by_token(request):
    try:
        token = (re.search('.+ (.+)', request.META.get('HTTP_AUTHORIZATION')))
        return Account.objects.get(id=request.data['id'], auth_token=token.group(1))
    except Account.DoesNotExist as e:
        logger.exception(f'Account.DoesNotExist! {e}')
    return ''


def _update_online_user(account):
    account.last_login = timezone.now()
    account.save()
