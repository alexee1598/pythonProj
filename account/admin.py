from django.contrib import admin

from account.models import Account, Image
from store.admin import export_to_csv


@admin.register(Account)
class CarAdmin(admin.ModelAdmin):
    list_display = ('id', 'username', 'first_name', 'last_name', 'age', 'description',
                    'email', 'is_staff', 'is_active', 'date_joined',)
    list_display_links = ('username', 'email',)
    search_fields = ('username', 'first_name', 'email',)
    actions = [export_to_csv]


@admin.register(Image)
class ImageAdmin(admin.ModelAdmin):
    list_display = ('id', 'image', 'created', 'updated',)
    list_display_links = ('id',)

