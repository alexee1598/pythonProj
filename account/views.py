import logging

from channels.layers import get_channel_layer
from django.contrib.auth.hashers import check_password, make_password
from django.utils.http import urlsafe_base64_decode
from rest_auth.serializers import TokenSerializer
from rest_framework.authtoken.models import Token
from rest_framework.decorators import api_view, permission_classes
from rest_framework.permissions import AllowAny, IsAuthenticated
from rest_framework.response import Response

from account.helper import registration, register_user_with_social, check_valid_email, _update_account, \
    _delete_account, _get_account_by_token, _update_online_user
from account.models import SocialAuth, Account
from account.serializer import AccountSerializer
from account.tasks import send_email_registration_job
from account.tokens import account_activation_token

logger = logging.getLogger(__name__)


@api_view(['POST'])
def register_user(request):
    try:
        account = registration(request)
        token = Token.objects.create(user=account)
    except Exception as e:
        if 'account_account_username_key' in str(e) or 'account_account.username' in str(e):
            return Response({
                'result': 0,
                'error': 'Username must be unique',
            })
        return Response({
            'result': 0,
            'error': 'Email address must be unique',
        })
    send_email_registration_job.send(account.id)
    return Response({
        'result': 1,
        'token': token.key,
        'account': AccountSerializer(account).data,
    })


@api_view(['POST'])
def social_register_user(request):
    try:
        social = SocialAuth.objects.get(provider=request.data['provider'], social_id=request.data['id'])
        account = Account.objects.get(pk=social.user.id)
        _update_online_user(account)

        token = Token.objects.get(user=account)
    except Exception as e:
        try:
            account = Account.objects.get(email=request.data['email'])
            _update_online_user(account)
            register_user_with_social(request, account)

            token = Token.objects.get(user=account)
        except Account.DoesNotExist:
            account = registration(request)
            register_user_with_social(request, account)

            token = Token.objects.create(user=account)

    return Response({
        'result': 1,
        'token': token.key,
        'account': AccountSerializer(account).data,
    })


@api_view(['POST'])
def login(request):
    try:
        username = request.data['username']
        account = Account.objects.get(username=username)
        password = check_password(request.data['password'], account.password)

        token = Token.objects.get_or_create(user=account)
        print(TokenSerializer(token[0]).data)

        if password:
            return Response({
                'account': AccountSerializer(account).data,
                'result': 1,
                'token': TokenSerializer(token[0]).data,
            })
        else:
            return Response({
                "result": 0,
                "error": 'Incorrect Password',
            })

    except Exception as e:
        print(e)
        return Response({
            "result": 0,
            "error": 'Invalid Username',
        })


@api_view(['PUT', 'DELETE'])
@permission_classes([IsAuthenticated])
def account_view(request):
    account = _get_account_by_token(request)
    if request.method == 'PUT' and account != '':
        return _update_account(account, request)
    elif request.method == 'DELETE' and account != '':
        return _delete_account(account)


@api_view(['POST'])
@permission_classes([IsAuthenticated])
def change_password(request):
    account = _get_account_by_token(request)
    password = check_password(request.data['old_password'], account.password)
    if password:
        account.password = make_password(request.data['new_password'])
        account.save()
        return Response({
            "result": 1,
            "error": ''
        })
    else:
        return Response({
            "result": 0,
            "error": 'Incorrect password'
        })


@api_view(['POST'])
@permission_classes([IsAuthenticated])
def change_email(request):
    account = _get_account_by_token(request)
    password = check_password(request.data['password'], account.password)
    if password:
        check_valid_email(request.data['new_email'])
        account.email = request.data['new_email']
        account.save()
    else:
        return Response({
            "result": 0,
            "error": "Incorrect password"
        })


@api_view(['GET'])
@permission_classes([AllowAny])
def get_account(request, account_id):
    channel_layer = get_channel_layer()
    print(channel_layer)
    account = Account.objects.get(id=account_id)

    return Response({
        "user": 1,
        "account": AccountSerializer(account).data
    })


@api_view(['GET'])
# @permission_classes([IsAuthenticated])
def activate(request, uid, token):
    try:
        uid = urlsafe_base64_decode(uid).decode()
        acc = Account.objects.get(pk=uid)
    except(TypeError, ValueError, OverflowError, Account.DoesNotExist):
        acc = None

    if acc is not None and account_activation_token.check_token(acc, token):
        acc.is_active = True
        acc.save()
        return Response({'Activation link is valid!'})
    else:
        return Response({'Activation link is invalid!'})
