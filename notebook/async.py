import os
import time
import asyncio
import aiohttp

from progress.bar import Bar

MAX_WORKERS = 10
BASE_DIR = './anime_images2/'
PAGE = 3000
link = Bar('Copy link', max=100)
urls = []
site_urls = []


async def get_image_url(session, url):
    print('url', url)
    print('session', session)
    async with session.get(url) as resp:
        print('here')
        print(resp.read())

        # print('2')
        # soup = BeautifulSoup(resp.text, 'html.parser')
        # body = soup.find('body')
        # link = Bar('Copy link', max=100)
        #
        # for post in body.find_all(class_="post-normal"):
        #     rating = float(post.find(class_='post_rating').text.strip())
        #     if rating < 6:
        #         continue
        #     else:
        #         print('3')
        #         content = post.find(class_='post_content')
        #         for img in content.find_all('img'):
        #             link.next()
        #             urls.append(img.get('src'))
        # link.finish()


async def url_many():
    async with aiohttp.ClientSession() as session:
        await asyncio.gather(*[asyncio.create_task(get_image_url(session, str(url))) for url in site_urls])


async def get_image(session, image_url):
    async with session.get(image_url) as resp:
        return await resp.read()


def set_image_name(image_url):
    index = int(float(image_url.rfind('.')))
    image_format = image_url[index + 1:: 1]
    index_slash = int(float(image_url.rfind('/')))
    name = image_url[index_slash + 1: index]
    return os.path.join(BASE_DIR, f'{name}.{image_format}')


def save_image(image, url):
    with open(set_image_name(url), "wb") as out:
        out.write(image)


async def download_one(session, url):
    image = await get_image(session, url)
    save_image(image, url)
    return url


async def download_many(urls):
    async with aiohttp.ClientSession() as session:  # <8>
        resp = await asyncio.gather(
            *[asyncio.create_task(download_one(session, cc)) for cc in urls])
    return len(resp)


if __name__ == "__main__":
    site_urls = ([f'http://anime.reactor.cc/{page}' for page in range(PAGE - 2, PAGE)])
    t0 = time.time()
    print(site_urls)
    asyncio.run(url_many())
    # count = asyncio.run(download_many(urls))
    elapsed = time.time() - t0
    print(urls)
    print(elapsed)
