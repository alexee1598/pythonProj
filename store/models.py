import datetime

from django.db import models

from account.models import Account


class Car(models.Model):
    brand = models.CharField(max_length=10, blank=False, default='')
    model = models.CharField(max_length=15, blank=False, default='')
    price = models.FloatField(max_length=9, blank=False, default=0)
    date = models.DateField(blank=False, default=datetime.date.today)
    owner = models.OneToOneField(Account, blank=True, null=True, on_delete=models.SET_NULL, related_name='owner')

    class Meta:
        verbose_name_plural = 'Cars'
        verbose_name = 'Car'
        ordering = ['brand']
