import datetime

from django.urls import reverse
from django_dramatiq.test import DramatiqTestCase
from rest_framework import status
from rest_framework.test import APITestCase, APIRequestFactory

from account.models import Account
from store.models import Car
from store.tasks import load_cars_job


class CarsCRUDTest(APITestCase):
    def setUp(self):
        self.car_id = 3000
        self.owner_id = 4000
        self.MAIN_URL = '/cars/'
        self.factory = APIRequestFactory()

        Car.objects.create(id=self.car_id, brand='BMW', model='Bgh22', price=12000, date=datetime.datetime.now())

    def test_get_cars(self):
        response = self.client.get(self.MAIN_URL, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertNotEqual(response.data, None)

    def test_get_car(self):
        response = self.client.get(f'{self.MAIN_URL}view/car/{self.car_id}/', format='json')
        print(response.data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertNotEqual(response.data, None)

    def test_incorrect_get_car(self):
        response = self.client.get(f'{self.MAIN_URL}view/car/{self.car_id + 1}/', format='json')
        self.assertEqual(response.data.get('result'), 0)

    def test_edit_car(self):
        response = self.client.put(f'{self.MAIN_URL}view/car/3000/', {'brand': 'Audi'}, format='json')
        self.assertEqual(response.data.get('result'), 1)
        car = Car.objects.get(id=self.car_id)
        self.assertEqual(car.brand, 'Audi')

    def test_incorrect_edit_car(self):
        response = self.client.put(f'{self.MAIN_URL}view/car/3000/', {'price': '30000000000sdfsdf'}, format='json')
        self.assertEquals(response.data.get('result'), 0)
        car = Car.objects.get(id=self.car_id)
        self.assertNotEqual(car.price, '30000000000sdfsdf')

    def test_delete_car(self):
        response = self.client.delete(f'{self.MAIN_URL}view/car/3000/', format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data.get('result'), 1)

    def test_get_owners_for_create_car(self):
        response = self.client.get(reverse('car_view'), format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_create_car(self):
        response = self.client.post(reverse('car_view'),
                                    {'brand': 'Audi', 'model': 'A8', 'price': 30000,
                                     'data': datetime.datetime.now()}, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data.get('result'), 1)

    def test_incorrect_create_car(self):
        response = self.client.post(reverse('car_view'),
                                    {'brand': 'Audi', 'model': 'A8', 'price': 'test',
                                     'data': datetime.datetime.now()}, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data.get('result'), 0)

    def test_get_owners(self):
        response = self.client.get(reverse('car_owners'), format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_scrapper_cars_without_authentication(self):
        response = self.client.get(reverse('scraper_car'), format='json')
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)
        self.assertEqual(response.data.get('detail'), "Authentication credentials were not provided.")

    def test_scrapper_cars_authentication(self):
        user_response = self.client.post(reverse('registration'),
                                         {'username': 'TESTER', 'password': 'qwerty1234',
                                          'age': 23, 'email': 'alex@gmail.com'}, format='json')
        token = f"Token {user_response.data['token']}"
        account_id = user_response.data['account']['id']
        test = Account.objects.get(pk=account_id)
        test.is_active = True
        test.save()
        response = self.client.get(reverse('scraper_car'), HTTP_AUTHORIZATION=token)
        self.assertEqual(response.data.get('result'), 1)
        self.assertEqual(response.status_code, status.HTTP_200_OK)


class CarsTestCase(DramatiqTestCase):

    def test_cars_can_be_scrappers(self):
        load_cars_job.send()

        self.broker.join(load_cars_job.queue_name)
        self.worker.join()
        self.assertEqual(Car.objects.all().count(), 12)
