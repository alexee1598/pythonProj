from rest_framework import serializers

from account.models import Account
from store.models import Car


class CarsSerializer(serializers.ModelSerializer):
    class Meta:
        model = Car
        fields = ('id', 'model', 'brand', 'price', 'date', 'owner')


class OwnerSerializer(serializers.ModelSerializer):
    class Meta:
        model = Account
        fields = ('id', 'first_name', 'last_name')


class CarsRelatedSerializer(serializers.ModelSerializer):
    class Meta:
        model = Car
        fields = ('id', 'model', 'brand', 'price', 'date')


class OwnerWithRelatedCarsSerializer(serializers.ModelSerializer):
    cars = CarsRelatedSerializer(many=True)

    class Meta:
        model = Account
        fields = ('id', 'first_name', 'last_name', 'age', 'email', 'cars')
