import logging

from rest_framework.authentication import TokenAuthentication
from rest_framework.decorators import api_view, permission_classes, authentication_classes
from rest_framework.permissions import IsAuthenticated, AllowAny
from rest_framework.response import Response

from store.helper import _get_car_for_edit, _update_car, _get_owners, _create_car, _delete_car, _get_cars_with_owner
from store.models import Car
from store.serializers import CarsSerializer
from store.tasks import load_cars_job

logger = logging.getLogger(__name__)


@api_view(['GET'])
@permission_classes([AllowAny])
def cars_list(request):
    cars_serializer = CarsSerializer(Car.objects.all(), many=True)
    return Response({
        "cars": cars_serializer.data,
        "result": 1,
        "error": ''
    })


@api_view(['GET', 'POST', 'PUT', 'DELETE'])
@permission_classes([AllowAny])
def car_view(request, car_id=None):
    if car_id is not None:
        try:
            car = Car.objects.get(id=car_id)
        except Car.DoesNotExist as e:
            logger.error(f'Car.DoesNotExist! {e}')
            return Response({
                "result": 0,
                "error": 'Car does not exist'
            })
        if request.method == 'GET':
            return _get_car_for_edit(car)
        elif request.method == 'PUT':
            return _update_car(car, request)
        elif request.method == 'DELETE':
            return _delete_car(car)
    else:
        if request.method == 'GET':
            return _get_owners()
        elif request.method == 'POST':
            return _create_car(request)


@api_view(['GET'])
@permission_classes([AllowAny])
def get_store_data(request):
    return _get_cars_with_owner()


@api_view(['GET'])
@authentication_classes([TokenAuthentication])
@permission_classes([IsAuthenticated])
def scraper_car(request):
    load_cars_job.send()
    return Response({
        "result": 1,
        "error": ""
    })
