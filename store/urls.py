from django.urls import path

from store import views

urlpatterns = [
    path('', views.cars_list),
    path('view/car/<int:car_id>/', views.car_view, name='car_view_id'),
    path('view/car/', views.car_view, name='car_view'),
    path('owners/', views.get_store_data, name='car_owners'),
    path('scraper_cars/', views.scraper_car, name='scraper_car'),
]
