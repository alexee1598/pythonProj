from venv import logger

import dramatiq
from rest_framework.response import Response

from ProjectDjango.celery import app
from store.helper import _scraper_cars


@dramatiq.actor
def load_cars_job():
    try:
        _scraper_cars()
        logger.info('All ok')
    except Exception as e:
        logger.error(f'Error - {e}')
        return Response({
            "result": 0,
            "error": 'error'
        })


@app.task(bind=True, default_retry_delay=300, max_retries=5)
def adding_task(self):
    try:
        return _scraper_cars()
    except Exception as exc:
        print(f'load_cars Exception - {exc}')
