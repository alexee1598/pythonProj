import csv
import datetime
import logging

from django.contrib import admin
from django.http import HttpResponse

from store.models import Car

logger = logging.getLogger(__name__)


def export_to_csv(modeladmin, request, queryset):
    opts = modeladmin.model._meta
    content_disposition = f'attachment; filename={opts.verbose_name}.csv'
    response = HttpResponse(content_type='text/csv')
    response['Content-Disposition'] = content_disposition
    writer = csv.writer(response)

    try:
        fields = [field for field in opts.get_fields() if not field.many_to_many and not field.one_to_many]

        # Write a first row with header information
        writer.writerow([field.name for field in fields])
        # Write data rows
        for obj in queryset:
            data_row = []
            for field in fields:
                try:
                    value = getattr(obj, field.name)
                    if isinstance(value, datetime.datetime):
                        value = value.strftime('%d/%m/%Y')
                    data_row.append(value)
                except Exception as e:
                    data_row.append('')
                    logger.error(e)
            writer.writerow(data_row)
    except Exception as e:
        logger.error(e)
    return response


export_to_csv.short_description = 'Export to CSV'


@admin.register(Car)
class CarAdmin(admin.ModelAdmin):
    list_display = ('id', 'brand', 'model', 'price', 'date', 'owner_id')
    list_display_links = ('brand', 'model')
    search_fields = ('date', 'model')
    actions = [export_to_csv]
